package aplicacion;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import javax.sql.rowset.WebRowSet;

public class EjemploWebRowSet {

	// fichero donde se guarda el CachedRowSet serializado
	private final static String ARCHIVO_CRS = "datoswrs.xml";
	private final static String IP = "192.168.56.103";

	public static void main(String[] args) {
		// Creamos un objeto CacheRowSetImpl serializado en un fichero
		String jdbcUrl = "jdbc:mariadb://" + IP + ":3306/empresa";
		String usr = "batoi";
		String pw = "1234";

		// Instanciamos un objeto WebRowSetImpl y parametrizamos la conexion
		try (WebRowSet wrs = RowSetProvider.newFactory().createWebRowSet()) {
			
			wrs.setUrl(jdbcUrl);
			wrs.setUsername(usr);
			wrs.setPassword(pw);
			
			wrs.setCommand("select * from articulo");
			// crs.setKeyColumns(new int[] { 1 }); // podemos indicar que columna/s 
			// forman parte de la clave (no obligatorio)
			wrs.execute();
			mostrar(wrs);

			// Lo serializamos en un fichero xml
			System.out.println("Serializando...");
			serializaWebRowSet(wrs);
			
			// Puedes abrir manualmente el archivo .xml generado y echar un vistazo a su estructura: puedes observar los datos y los metadados.		

		} catch (SQLException se) {
			System.err.println(se.getMessage());
		}
		
		// Leyendo de archivo serializado y poblando otro webrowset (wrs2)
		try  (WebRowSet wrs2 = RowSetProvider.newFactory().createWebRowSet()) {
			
			wrs2.setUrl(jdbcUrl);
			wrs2.setUsername(usr);
			wrs2.setPassword(pw);
			System.out.println("Deserializando...");
			leeWebRowSet(wrs2);
			wrs2.first();
			wrs2.updateFloat(3, 125);
			wrs2.updateRow();
			wrs2.acceptChanges();
			mostrar(wrs2);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		
	}
	
	public static void leeWebRowSet(WebRowSet wrs) {
		try (FileInputStream fis = new FileInputStream(ARCHIVO_CRS)) {
			wrs.readXml(fis);
		} catch (FileNotFoundException ex) {
			System.err.println("Fichero no existe: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error E/S: " + ex.getMessage());
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}


	private static void serializaWebRowSet(WebRowSet crs) {

		try (FileOutputStream fos = new FileOutputStream(ARCHIVO_CRS, false)) {
			crs.writeXml(fos);
			System.out.println("¡¡Serializado con éxito!!");
			System.out.println("--------------------------------------");
		} catch (IOException | SQLException ex) {
			System.err.println(ex.getMessage());
		}
	}

	private static void mostrar(CachedRowSet crs) throws SQLException {
		if (crs != null) {
			crs.beforeFirst();
			while (crs.next()) {
				System.out.print("ID: " + crs.getInt("id"));
				System.out.print(", Nombre: " + crs.getString("nombre"));
				System.out.print(", Precio: " + crs.getFloat("precio"));
				System.out.print(", Codigo: " + crs.getString("codigo"));
				System.out.print(", Grupo: " + crs.getInt("grupo"));
				System.out.print(", Stock: " + crs.getInt("stock"));
				System.out.println();
			}
		} else {
			System.out.println("CachedRowSet es null");
		}
		System.out.println("--------------------------------------");
	}
}
