package aplicacion;

import java.io.*;
import java.sql.SQLException;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;

public class EjemploCachedRowSet {

	// fichero donde se guarda el CachedRowSet serializado
	private final static String ARCHIVO_CRS = "datoscrs.dat";
	final static String IP = "192.168.56.103";

	// Creación de un objeto CacheRowSet serializado en un fichero
	public static void main(String[] args) {

//		String jdbcUrl = "jdbc:mariadb://" + IP + ":3306/empresa";
//		String jdbcUrl = "jdbc:postgresql://" + IP + ":5432/batoi?currentSchema=empresa";
//		String usr = "batoi";
//		String pw = "1234";

		// Instanciamos un objeto CachedRowSet y parametrizamos la conexion
		try (CachedRowSet crs = RowSetProvider.newFactory().createCachedRowSet()) {

			// Para operaciones de manipulación (postgresql obliga a modo transacción, sino no funciona)
//			ConexionDS.getConexion().setAutoCommit(false);

			// Sería posible transformar un Resultset en un cachedrowset.
			// ResultSet rs = ConexionBD.getConexion().createStatement().executeQuery("SELECT * FROM articulo");
			// crs.populate(rs);

//			crs.setUrl(jdbcUrl);
//			crs.setUsername(usr);
//			crs.setPassword(pw);

			// poblamos el cachedrowset
			crs.setCommand("select * from articulo");
			crs.setKeyColumns(new int[] { 1 }); // podemos indicar que columna/s forman parte de la clave (no obligatorio)
//			crs.execute();
			crs.execute(ConexionDS.getConexion());
			mostrar(crs);

			// ------------------- TRABAJO OFFLINE ------------------------------
			
			// inserción
			System.out.println("insertando...");			
			crs.moveToInsertRow();
			crs.updateNull(1);		// en postgresql no genera bien el auto incrementado: habría que buscar alternativa.
//			crs.updateInt(1, ...);
			crs.updateString(2, "arti1");
			crs.updateFloat(3, 100f);
			crs.updateString(4, "art1x");
			crs.updateInt(5, 1);
			crs.updateInt(6, 100);
			crs.insertRow();
			crs.moveToCurrentRow();
//			crs.acceptChanges();
			
			// modificación
			crs.first();
			crs.updateFloat("precio", crs.getFloat(("precio")) + 200);
			crs.updateInt("grupo", 2);
			crs.updateRow();
			
			// SINCRONIZACIÓN con la fuente de datos			
			crs.acceptChanges();
//			crs.acceptChanges(ConexionDS.getConexion());

			mostrar(crs);
			
//			ConexionDS.getConexion().setAutoCommit(true);
			
			// ------------------- SERIALIZACIÓN ------------------------------
			
			// Lo serializamos en un fichero
			System.out.println("Serializando...");
			serializa(crs);

			// Leyendo de archivo serializado...
			System.out.println("Deserializando...");
			CachedRowSet crs2 = deserializa();
			mostrar(crs2);

		} catch (SQLException se) {
			System.err.println(se.getMessage());
		}
	}

	private static void serializa(CachedRowSet crs) {

		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(ARCHIVO_CRS, false))) {
			oos.writeObject(crs);
			System.out.println("¡¡Serializado con éxito!!");
			System.out.println("--------------------------------------");
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}
	}

	public static CachedRowSet deserializa() {
		CachedRowSet crs = null;
		// Leemos el objeto CachedRowSetImpl serializado desde el archivo
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(ARCHIVO_CRS))) {
			crs = (CachedRowSet) ois.readObject();
		} catch (FileNotFoundException ex) {
			System.out.println("Fichero no existe: " + ex.getMessage());
		} catch (IOException | ClassNotFoundException ex) {
			System.err.println("Error E/S: " + ex.getMessage());
		}
		return crs;
	}

	private static void mostrar(CachedRowSet crs) throws SQLException {
		if (crs != null) {
			crs.beforeFirst();
			while (crs.next()) {
				System.out.print("ID: " + crs.getInt("id"));
				System.out.print(", Nombre: " + crs.getString("nombre"));
				System.out.print(", Precio: " + crs.getFloat("precio"));
				System.out.print(", Codigo: " + crs.getString("codigo"));
				System.out.print(", Grupo: " + crs.getInt("grupo"));
				System.out.print(", Stock: " + crs.getInt("stock"));
				System.out.println();
			}
		} else {
			System.out.println("CachedRowSet es null");
		}
		System.out.println("--------------------------------------");
	}
}
