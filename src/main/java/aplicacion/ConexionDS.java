package aplicacion;

import java.sql.Connection;
import java.sql.SQLException;

import org.mariadb.jdbc.MariaDbDataSource;
import org.postgresql.ds.PGSimpleDataSource;

// Patrón singleton
public class ConexionDS {

	private static Connection con = null;
	private final static String IP = "192.168.56.103";
	private final static String DB = "empresa";

	public static Connection getConexion() throws SQLException {
		if (con == null) {
			// Instanciamos el objeto DataSource
			
			// Mariadb
			MariaDbDataSource ds = new MariaDbDataSource(); 
			ds.setUrl("jdbc:mariadb://" + IP + ":3306/" + DB);
			ds.setUser("batoi");
			ds.setPassword("1234");
			

			// Postgresql
//			PGSimpleDataSource ds = new PGSimpleDataSource();
//			ds.setUrl("jdbc:postgresql://" + IP + ":5432/batoi?currentSchema=" + DB);
//			ds.setDatabaseName("batoi"); 
//			ds.setCurrentSchema(DB);
//			ds.setUser("batoi");
//			ds.setPassword("1234");

			con = ds.getConnection();
//			con = ds.getConnection("batoi", "1234");
		}
		return con;
	}

	public static void cerrar() throws SQLException {
		if (con != null) {
			con.close();
			con = null;
		}
	}

}
