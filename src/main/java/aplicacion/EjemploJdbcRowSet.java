package aplicacion;

import java.sql.SQLException;

import javax.sql.RowSet;
import javax.sql.RowSetEvent;
import javax.sql.RowSetListener;
import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

public class EjemploJdbcRowSet {
	final static String IP = "192.168.56.103";

	public static void main(String[] args) throws InterruptedException {

		String jdbcUrl = "jdbc:mariadb://" + IP + ":3306/empresa";
//		String jdbcUrl = "jdbc:postgresql://" + IP + ":5432/batoi?currentSchema=empresa";
		String usr = "batoi";
		String pw = "1234";

		try (JdbcRowSet jrs = RowSetProvider.newFactory().createJdbcRowSet()) {
			jrs.setUrl(jdbcUrl);
			jrs.setUsername(usr);
			jrs.setPassword(pw);

			// Añadir listener
			anyadirListener(jrs);

			// Consultas
			jrs.setCommand("select * from empresa.cliente");
			jrs.execute();
			mostrar(jrs);

			// Inserción
			System.out.println("Insertando...");
			jrs.moveToInsertRow();
			jrs.updateString(2, "Sergio");
			jrs.updateString(3, "C/ laquesea, 2");
			jrs.insertRow();
			jrs.moveToCurrentRow();
			System.out.println("Fila insertada!");
			mostrar(jrs);

			// Modificación
			System.out.println("Modificando...");
			jrs.last();
			jrs.updateString(2, "Sergio modificado");
			jrs.updateRow();
			System.out.println("Fila actualizada!");
			mostrar(jrs);

			// Borrado
			System.out.println("Borrando...");
			jrs.last();
			jrs.deleteRow();
			System.out.println("Fila borrada!");
			mostrar(jrs);

			System.out.println("Consulta parametrizada");
			jrs.setCommand("select * from empresa.cliente where nombre like ?");
			jrs.setString(1, "John%");
			jrs.execute();

			mostrar(jrs);

		} catch (SQLException se) {
			System.err.println("Error: " + se.getMessage());
		}
	}

	
	// RowSet pueden notificar cambios a otros componentes mediante event listeners.
	private static void anyadirListener(JdbcRowSet jrs) {

		jrs.addRowSetListener(new RowSetListener() {
			@Override
			public void rowSetChanged(RowSetEvent event) {
				System.out.println("\t \u001B[32m" + "Rowset manipulado!" + "\u001B[0m");
			}

			@Override
			public void rowChanged(RowSetEvent event) {
				JdbcRowSet j = (JdbcRowSet) event.getSource();
				try {
					RowSet rws = (JdbcRowSet) event.getSource();
					if (rws.rowUpdated()) {
						System.out.println("\t \u001B[32m" + "Fila modificada! -> Posicion: " + j.getRow() + "\u001B[0m");
					} else if (rws.rowInserted()) {
						System.out.println("\t \u001B[32m" + "Fila insertada! -> " + j.getRow() + "\u001B[0m");
					} else {
						System.out.println("\t \u001B[32m" + "Fila borrada! -> " + j.getRow() + "\u001B[0m");
					}
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}

			@Override
			public void cursorMoved(RowSetEvent event) {
				JdbcRowSet j = (JdbcRowSet) event.getSource();
				try {
					System.out.println("\t \u001B[32m" + "Cursor se ha movido! -> " + j.getRow() + "\u001B[0m");
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		});

	}

	private static void mostrar(JdbcRowSet jrs) throws SQLException {
		jrs.beforeFirst();
		while (jrs.next()) {
			System.out.print("ID: " + jrs.getInt(1));
			System.out.print(", Nombre: " + jrs.getString(2));
			System.out.println();
		}
		System.out.println("------------------------------------------------");
	}
}
