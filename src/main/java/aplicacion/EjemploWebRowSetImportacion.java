package aplicacion;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;
import javax.sql.rowset.WebRowSet;

public class EjemploWebRowSetImportacion {

	// fichero donde se guarda el CachedRowSet serializado
	private final static String ARCHIVO_CRS = "datoswrs.xml";
	final static String IP = "192.168.56.101";

	public static void main(String[] args) {
		String jdbcUrl = "jdbc:mariadb://" + IP + ":3306/empresa_ad";
		String usr = "batoi";
		String pw = "1234";

		// Leyendo de archivo serializado...
		try (WebRowSet wrs2 = RowSetProvider.newFactory().createWebRowSet()) {

			wrs2.setUrl(jdbcUrl);
			wrs2.setUsername(usr);
			wrs2.setPassword(pw);
			System.out.println("Deserializando...");
			leeWebRowSet(wrs2);
			wrs2.first();
			wrs2.updateFloat(3, 135);
			wrs2.updateRow();
			wrs2.acceptChanges();
			mostrar(wrs2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void leeWebRowSet(WebRowSet wrs) {
		try (FileInputStream fis = new FileInputStream(ARCHIVO_CRS)) {
			wrs.readXml(fis);
		} catch (FileNotFoundException ex) {
			System.err.println("Fichero no existe: " + ex.getMessage());
		} catch (IOException ex) {
			System.err.println("Error E/S: " + ex.getMessage());
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}

	private static void mostrar(CachedRowSet crs) throws SQLException {
		if (crs != null) {
			crs.beforeFirst();
			while (crs.next()) {
				System.out.print("ID: " + crs.getInt("id"));
				System.out.print(", Nombre: " + crs.getString("nombre"));
				System.out.print(", Precio: " + crs.getFloat("precio"));
				System.out.print(", Codigo: " + crs.getString("codigo"));
				System.out.print(", Grupo: " + crs.getInt("grupo"));
				System.out.println();
			}
		} else {
			System.out.println("CachedRowSet es null");
		}
		System.out.println("--------------------------------------");
	}
}
